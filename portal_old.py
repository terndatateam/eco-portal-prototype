from flask import Flask, render_template, request
import requests
from rdflib import Graph, URIRef
from rdflib.namespace import SKOS
from jinja2 import Undefined

import pprint as pp
import pickle

app = Flask(__name__)


def render(value):
    # TODO: Display this nicely
    return value

    if isinstance(value, str):
        return value

    if isinstance(value, list):
        result = ''
        for item in value:
            if isinstance(item, dict):
                if item.get('@type') and item.get('@value'):
                    result += 'TYPE: {} VALUE: {}, '.format(item.get('@type'), item.get('@value'))
        return result


def get_label(uri: str, g: Graph):
    for label in g.objects(URIRef(uri), SKOS.prefLabel):
        return label


def get_elasticsearch_index_schema(url: str) -> dict:
    return requests.get(url).json()


def get_text_fields(headers: list) -> list:
    """Get fields of an Elasticsearch document where the values are text."""
    text_fields = []
    for header in headers:
        if headers[header].get('type') == 'text':
            text_fields.append(header)
    return text_fields


def load_vocabs(rdf_filename, pickle_filename) -> Graph:
    try:
        with open(pickle_filename, 'rb') as f:
            g = pickle.load(f)
    except:
        with open(pickle_filename, 'wb') as f:
            g = Graph().parse(rdf_filename, format='turtle')
            pickle.dump(g, f)
    return g


def get_controlled_fields(headers: list) -> list:
    """Get fields of an Elasticsearch document where the values are JSON objects."""
    controlled_fields = []
    for header in headers:
        if headers[header].get('properties'):
            if headers[header].get('properties').get('@id'):
                controlled_fields.append(header)
    return controlled_fields


def get_faceted_field_values(controlled_fields: list, g: Graph) -> dict:
    """Get the faceted fields of an Elasticsearch document with their values."""
    faceted_field_values = {}
    for field in controlled_fields:
        if field in [
            'locn:Location',
            'plot:siteDescription',
            'prov:wasGeneratedBy'
        ]:
            continue
        r = requests.get('http://localhost:9200/site/_doc/_search', json={
            'size': 0,
            'aggs': {
                field: {
                    'terms': {
                        'field': '{}.@id.keyword'.format(field)
                    }
                }
            }
        }).json()

        # Get human-readable values
        values = r.get('aggregations').get(field).get('buckets')
        for value in values:
            label = get_label(value['key'], g)
            value.update({'label': label})

        faceted_field_values.update({field: values})
        # break # TODO: Remove - currently limiting to just one faceted search field.
    return faceted_field_values


@app.route('/')
def home():
    headers = get_elasticsearch_index_schema('http://localhost:9200/site')['site']['mappings']['properties']

    # Document fields with values as straight text.
    text_fields = get_text_fields(headers)

    # Load vocabs to retrieve human-readable labels
    g = load_vocabs('CORVEG.ttl', 'corveg.p')

    # Document fields where values are a JSON object with properties.
    controlled_fields = get_controlled_fields(headers)

    faceted_field_values = get_faceted_field_values(controlled_fields, g)

    selected_faceted_values = []
    for key in faceted_field_values:
        values = request.values.getlist(key)
        selected_faceted_values.append({'facet': key, 'values': values})

    paginate_from = int(request.values.get('from')) if request.values.get('from') else 0
    paginate_size = int(request.values.get('size')) if request.values.get('size') else 100

    query_body = {
        'from': paginate_from,
        'size': paginate_size,
        'query': {
            'bool': {
                'should': [
                ]
            }
        }
    }

    for field in selected_faceted_values:
        for value in field['values']:
            query_body['query']['bool']['should'].append({'match_phrase': {'{}.@id'.format(field['facet']): value}})

    query = request.values.get('query')
    if query:
        query_body['query']['bool']['should'].append({'multi_match': {'query': query}})

    r = requests.get('http://localhost:9200/site2/_doc/_search', json=query_body).json()

    rows = r['hits']['hits']
    total = r['hits']['total']['value']
    took = r['took']

    selected_values = []
    for field in selected_faceted_values:
        for value in field['values']:
            selected_values.append(value)

    return render_template('index.html',
                           title='Ecological Data Portal',
                           p1="Proof-of-concept - using Elasticsearch to power TERN's Ecological Data Portal.",
                           p2="Created by TERN Data Services and Analytics.",
                           took=took,
                           total=total,
                           query=query,
                           headers=headers,
                           rows=rows,
                           faceted_field_values=faceted_field_values,
                           selected_values=selected_values,
                           # g=g,
                           render=render,
                           paginate_from=paginate_from,
                           paginate_size=paginate_size)


if __name__ == '__main__':
    app.run(debug=True)
