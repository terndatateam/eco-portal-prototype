from rdflib import URIRef
from rdflib.namespace import RDF
from tern_rdf import TernRdf, PLOT
from elasticsearch import Elasticsearch
import json
import rdflib_jsonld.serializer

import pprint as pp


if __name__ == '__main__':
    g1 = TernRdf.Graph().parse('site-sample-data.ttl', format='turtle')
    g2 = TernRdf.Graph()

    sites = []
    for site in g1.subjects(RDF.type, PLOT.Site):
        sites.append(site)
        # if len(sites) == 10:
        #     break

    for site in sites:
        for s, p, o in g1.triples((site, None, None)):
            g2.add((s, p, o))

    results = g2.serialize(format='json-ld', auto_compact=True).decode('utf-8')
    g2.serialize(destination='sites.jsonld', format='json-ld', auto_compact=True)
    results = json.loads(results)
    # pp.pprint(results)

    es = Elasticsearch()

    for body in results['@graph']:
        id = body['@id']
        es.index(index='site', doc_type='_doc', id=id, body=body)

    print(sites)
    print('Success!')
