from flask import Flask, render_template, request, Response, jsonify
import requests
from rdflib import Graph, URIRef
from rdflib.namespace import SKOS

import headers_def

import pickle
import math
from urllib.parse import unquote_plus
from uuid import uuid4
import csv
import os

app = Flask(__name__)

ELASTICSEARCH_URL = 'https://es-test.tern.org.au'
APP_DIR = os.path.dirname(os.path.realpath(__file__))


def render(data):
    try:
        if data['type'] == 'literal':
            return data['value']
        elif data['type'] == 'datetime':
            return data['value']
        elif data['type'] == 'controlled':
            if data['label'] is not None:
                return '<a target="_blank" href=http://linkeddata.tern.org.au/viewer/corveg/id/{}>{}</a>'.format(data['value'], data['label'])
            else:
                return '<a target="_blank" href={0}>{0}</a>'.format(data['value'])
    except:
        # Field missing in document, skip
        return ''


def get_label(uri: str, g: Graph):
    for label in g.objects(URIRef(uri), SKOS.prefLabel):
        return label


def load_vocabs(rdf_filename, pickle_filename) -> Graph:
    try:
        with open(pickle_filename, 'rb') as f:
            g = pickle.load(f)
    except:
        with open(pickle_filename, 'wb') as f:
            g = Graph().parse(rdf_filename, format='xml')
            pickle.dump(g, f)
    return g


def field_is_controlled_vocab(field, index, base_es_url):
    r = requests.get('{}/{}/_doc/_search'.format(base_es_url, index), json={
        "size": 0,
        "aggs": {
            "query": {
                "terms": {
                    "field": "{}.type.keyword".format(field)
                }
            }
        }
    })
    types = [item['key'] for item in r.json()['aggregations']['query']['buckets']]
    # if len(types) > 1:
    #     raise Exception('The field "{}" in this document has more than one type. Expecting only one type. Types: {}'.format(field, types))
    # elif len(types) == 0:
    #     # raise Exception('The field "{}" in this document is not assigned a type.'.format(field))
    #     return False
    if not types:
        return False
    return types[0] == 'controlled'


def get_unique_values_of_field(field, index, base_es_url, g):
    r = requests.get('{}/{}/_doc/_search'.format(base_es_url, index), json={
        "size": 0,
        "aggs": {
            "query": {
                "terms": {
                    "field": "{}.value.keyword".format(field),
                    "size": 5000
                }
            }
        }
    })
    labelled_items = []
    items = [item['key'] for item in r.json()['aggregations']['query']['buckets']]
    for item in items:
        label = get_label(item, g)
        labelled_items.append({'uri': str(item), 'label': str(label)})
    return labelled_items


def get_controlled_fields(es_url, index, headers: list, g: Graph) -> dict:
    """Get fields of an Elasticsearch document where the values are JSON objects."""
    controlled_fields = {}
    for header in headers:
        is_field_controlled = field_is_controlled_vocab(header, index, es_url)
        if is_field_controlled:
            unique_values = get_unique_values_of_field(header, index, es_url, g)
            controlled_fields.update({header: unique_values})
    return controlled_fields


def get_headers_order(headers, rows):
    ordered_headers = []
    for row in rows:
        ordered_headers = [header for header in row['_source']]
        missing = False
        for header in headers:
            if not header in ordered_headers:
                # A header is missing in this document
                missing = True
                break
        if missing:
            # Missing header, go next
            continue

        # All headers exist, success
        break
    return ordered_headers


def get_total_pages(total_rows, size):
    return math.ceil(total_rows / size)


def get_headers(es_url, index, rows):
    headers = requests.get('{}/{}'.format(es_url, index)).json()[index]['mappings']['properties']
    headers = get_headers_order(headers, rows)
    return headers


def format_pagination_url(url, page):
    url = unquote_plus(url)

    if '?page=' in url:
        # The 'page' query string argument is the first parameter with '?'
        splits = url.split('?')
        if '&' in splits[1]:
            splits = splits[1].split('&')
            url = url.replace(splits[0], 'page={}'.format(page))
        else:
            url = url.replace(splits[1], 'page={}'.format(page))
        return url

    if '&page' in url:
        splits = url.split('&')
        for split in splits:
            if 'page' in split:
                url = url.replace(split, 'page={}'.format(page))
    else:
        # Add &page to url as a query string arg.
        url = '{}&page={}'.format(url, page)

    if not '?' in url:
        # No pre-existing query string argument
        url = url.replace('&page=', '?page=')

    return url


def get_headers2(theme: str):
    """
    This replaced the other function get_headers because there are rows where not all header columns will be
    available, and will not get the full set of headers.
    """
    if theme == 'site':
        return headers_def.site
    elif theme == 'disturbance':
        return headers_def.disturbance
    else:
        raise Exception('Invalid theme: {}'.format(theme))


def get_unselected_headers(ops, headers):
    """
    Get the headers of the observed properties that the user deselected
    :return:
    """
    deselect = []
    for op in ops:
        for key in headers_def.disturbance_inverted_op:
            if key == op:
                deselect += headers_def.disturbance_inverted_op[op]

    deselect = list(set(deselect))

    default_ops = []
    values = headers_def.disturbance_inverted_op.values()
    for value in values:
        default_ops += value

    new_headers = []

    for header in headers:
        if header not in default_ops:
            new_headers.append(header)

    for header in headers:
        if header in deselect:
            new_headers.append(header)

    return new_headers


def get_download_url(request):
    url = request.url
    url = url.replace(request.url_root, '')
    url = '/download' + url
    return url


def render_theme(theme: str, template: str, request: request):
    page = int(request.values.get('page')) if request.values.get('page') else 1
    paginate_size = 20
    paginate_from = (page - 1) * paginate_size

    headers = get_headers2(theme)
    ops = request.values.getlist('op')
    headers = get_unselected_headers(ops, headers)

    # Load vocabs to retrieve human-readable labels
    g = load_vocabs(os.path.join(APP_DIR, 'Queensland CORVEG Database Vocabularies.rdf'), 'corveg.p')

    controlled_fields = get_controlled_fields(ELASTICSEARCH_URL, theme, headers, g)

    selected_faceted_values = []
    for facet in controlled_fields:
        values = request.values.getlist(facet)
        selected_faceted_values.append({'facet': facet, 'values': values})

    query_body = {
        'from': paginate_from,
        'size': paginate_size,
        "track_total_hits": True,
        'query': {
            'bool': {
                'must': []
            }
        }
    }

    for field in selected_faceted_values:
        entry = {"bool": {"should": []}}
        for value in field['values']:
            entry['bool']['should'].append({'match_phrase': {'{}.value'.format(field['facet']): value}})
        query_body['query']['bool']['must'].append(entry)

    query = request.values.get('query')
    if query:
        query_body['query']['bool']['must'].append({'multi_match': {'query': query}})
    else:
        query = ''

    r = requests.get('{}/{}/_doc/_search'.format(ELASTICSEARCH_URL, theme), json=query_body).json()
    rows = r['hits']['hits']
    total = r['hits']['total']['value']
    took = r['took']

    selected_values = {}
    for field in selected_faceted_values:
        for value in field['values']:
            if selected_values.get(field['facet']):
                selected_values[field['facet']].append(value)
            else:
                selected_values.update({field['facet']: []})
                selected_values[field['facet']].append(value)

    class Object(object):
        pass

    h = Object
    h.render = render
    h.get_total_pages = get_total_pages
    h.format_pagination_url = format_pagination_url

    # Get download URL
    download_url = get_download_url(request)

    return render_template('index.html',
                           title='Ecological Data Portal',
                           p1="Proof-of-concept - using Elasticsearch to power TERN's Ecological Data Portal.",
                           p2="Created by TERN Data Services and Analytics.",
                           took=took,
                           total=total,
                           query=query,
                           headers=headers,
                           rows=rows,
                           selected_values=selected_values,
                           page=page,
                           paginate_from=paginate_from,
                           paginate_size=paginate_size,
                           request=request,
                           h=h,
                           controlled_fields=controlled_fields,
                           theme=theme,
                           ops=ops,
                           download_url=download_url
   )


def get_csv(theme):
    headers = get_headers2(theme)
    ops = request.values.getlist('op')
    headers = get_unselected_headers(ops, headers)

    # Load vocabs to retrieve human-readable labels
    g = load_vocabs('CORVEG.ttl', 'corveg.p')

    controlled_fields = get_controlled_fields(ELASTICSEARCH_URL, theme, headers, g)

    selected_faceted_values = []
    for facet in controlled_fields:
        values = request.values.getlist(facet)
        selected_faceted_values.append({'facet': facet, 'values': values})

    query_body = {
        "track_total_hits": True,
        'query': {
            'bool': {
                'must': []
            }
        }
    }

    for field in selected_faceted_values:
        entry = {"bool": {"should": []}}
        for value in field['values']:
            entry['bool']['should'].append({'match_phrase': {'{}.value'.format(field['facet']): value}})
        query_body['query']['bool']['must'].append(entry)

    query = request.values.get('query')
    if query:
        query_body['query']['bool']['must'].append({'multi_match': {'query': query}})
    else:
        query = ''

    r = requests.get('{}/{}/_doc/_search?scroll=1m'.format(ELASTICSEARCH_URL, theme), json=query_body).json()
    rows = r['hits']['hits']
    total = r['hits']['total']['value']
    took = r['took']
    print('total:', total, 'took:', took)
    while r.get('_scroll_id'):
        scroll_id = r['_scroll_id']
        r = requests.get('{}/_search/scroll'.format(ELASTICSEARCH_URL), json={'scroll_id': scroll_id, 'scroll': '1m'}).json()

        if not r['hits']['hits']:
            break

        rows += r['hits']['hits']

    # Serialize to a CSV file
    # Check if csv_files directory exists, if not, create it.
    csv_dir = os.path.join(APP_DIR, 'csv_files')
    if not os.path.isdir(csv_dir):
        os.makedirs(csv_dir)
    filename = '{}.csv'.format(os.path.join(APP_DIR, csv_dir, str(uuid4())))
    with open(filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=headers)
        writer.writeheader()

        for row in rows:
            data = {}
            for key in row.get('_source'):
                if key in headers:
                    data.update({key: row['_source'][key]['value']})
            writer.writerow(data)

    with open(filename, 'r') as csvfile:
        csv_data = csvfile.read()

    return csv_data


@app.route('/')
def home():
    theme = request.values.get('theme')
    if theme is None:
        theme = 'site'
    # TODO: Remove template, it is unused.
    template = theme
    if template == 'site':
        template = 'index.html'
    else:
        template += '.html'

    return render_theme(theme, template, request)


@app.route('/download', methods=['GET', 'POST'])
def download():
    theme = request.values.get('theme')

    csv = get_csv(theme)

    return Response(
        csv,
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename={}.csv'.format(theme)}
    )


def test_str():
    return 'hello, test3!'


@app.route('/test')
def test():
    #import cProfile as profile
    #pr = profile.Profile()
    #pr.disable()

    #pr.enable()
    s = test_str()
    #pr.disable()
    #pr.dump_stats('profile.pstat')

    return s


if __name__ == '__main__':
    app.run(debug=True)

