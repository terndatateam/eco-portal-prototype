# Author:   Edmond Chuc

# Purpose:  A terms aggregation query in Elasticsearch using the Scroll API to fetch distinct field values for an index
#           that has more than 10000 documents.

from elasticsearch import Elasticsearch
import requests

import pprint as pp


def get_distinct_field_values(field, index, base_es_url):
    r = requests.get('{}/{}/_doc/_search'.format(base_es_url, index), json={
        "size": 0,
        "aggs": {
            "query": {
                "terms": {
                    "field": "{}.value.keyword".format(field),
                    "size": 10000
                }
            }
        }
    })

    results = r.json()['aggregations']['query']['buckets']
    pp.pprint(results)
    print('Total', len(results))

    total = 0
    for result in results:
        total += result['doc_count']
    print('Total documents', total)

    pp.pprint(r.json())


def get_documents(index, base_es_url):
    r = requests.get('{}/{}/_doc/_search'.format(base_es_url, index), json={
        "track_total_hits": True,
    })

    pp.pprint(r.json())
    print('Total', len(r.json()['hits']['hits']))


if __name__ == '__main__':
    # get_distinct_field_values('disturbance_type', 'disturbance', 'http://localhost:9200')
    get_documents('disturbance', 'http://localhost:9200')