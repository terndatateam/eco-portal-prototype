# Ecological Data Portal Prototype
[![Website](https://img.shields.io/website/http/plots-dev.tern.org.au?down_color=red&down_message=offline&label=plots-dev&style=flat-square&up_message=online)](http://plots-dev.tern.org.au)

This is a prototype of the TERN Ecological Data Portal, driven by Elasticsearch. 

The prototype showcases the auto-generation of the faceted search (terms aggregation) as well as the example result data of what it will likely look like in the *view*. 

Users can download the data in CSV format.

It is a server-side web application. The production version will likely be developed using a front-end library/framework to render the pages on the client side. 


## Contact
**Edmond Chuc**  
*Software Engineer*  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  

