from SPARQLWrapper import SPARQLWrapper, JSON
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import SKOS
from elasticsearch import Elasticsearch
from tqdm import tqdm

from queries import SITES_QUERY, OGROUP_DISTURBANCE_QUERY

import pprint as pp
from datetime import datetime
import pickle
import time


def load_vocabs(rdf_filename, pickle_filename) -> Graph:
    try:
        with open(pickle_filename, 'rb') as f:
            g = pickle.load(f)
    except:
        with open(pickle_filename, 'wb') as f:
            g = Graph().parse(rdf_filename, format='turtle')
            pickle.dump(g, f)
    return g


def get_label(uri: str, g: Graph):
    for label in g.objects(URIRef(uri), SKOS.prefLabel):
        return str(label)
    raise Exception('No label found for uri {}'.format(uri))


if __name__ == '__main__':
    starttime = time.time()

    print('Executing remote SPARQL query.. ', end='')
    sparql = SPARQLWrapper('http://graphdb-dev.tern.org.au/repositories/CORVEG-1')
    sparql.setQuery(SITES_QUERY)

    sparql.setReturnFormat(JSON)
    response = sparql.query().convert()
    print('done.')
    header = response['head']['vars']
    # pp.pprint(response['head']['vars'])
    # pp.pprint(response['results']['bindings'][0])

    # Load the vocabularies
    print('Loading RDF vocabularies from disk.. ', end='')
    g = load_vocabs('CORVEG.ttl', 'corveg.p')
    print('done.')

    print('Parsing JSON response object.. ', end='')
    documents = []
    for row in response['results']['bindings']:
        document = {}
        for head in header:
            # First check if there is a datatype field - we only know about dateTime - convert to dateTime object
            # If the type is uri - then assume it is a controlled vocabulary - look for the human-readable label
            # If it is a literal - then just add it and assume it is text or a number.
            try:
                if not row.get(head):
                    # If field is not in the document schema, then skip.
                    continue

                # if head == 'site_uri':
                #     # 'site_uri' field, retrieve the observation data about this site.
                #


                if row[head].get('datatype') == 'http://www.w3.org/2001/XMLSchema#dateTime':
                    document.update({
                        head: {
                            'type': 'datetime',
                            'value': str(datetime.fromisoformat(row[head]['value']))
                        }
                    })
                elif row[head]['type'] == 'uri':
                    label = get_label(row[head]['value'], g)
                    document.update({
                        head: {
                            'type': 'controlled',
                            'value': str(row[head]['value']),
                            'label': label
                        }
                    })
                else:
                    # It is a literal value (text/number).
                    document.update({
                        head: {
                            'type': 'literal',
                            'value': str(row[head]['value'])
                        }
                    })
            except Exception as e:
                raise Exception(e, row)
        documents.append(document)
    print('done.')

    # Index into Elasticsearch
    es = Elasticsearch()

    print('Indexing Elasticsearch documents.. ')
    for document in tqdm(documents):
        id = 'corveg-site-{}'.format(document['site_id']['value'])
        es.index(index='site2', doc_type='_doc', id=id, body=document)

    print('Completed indexing in {:.2f} seconds.'.format(time.time() - starttime))
