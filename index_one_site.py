from rdflib import URIRef
from rdflib.namespace import RDF
from tern_rdf import TernRdf, PLOT
from elasticsearch import Elasticsearch
import json


if __name__ == '__main__':
    g1 = TernRdf.Graph().parse('site-sample-data.ttl', format='turtle')
    g2 = TernRdf.Graph()

    for s, p, o in g1.triples((URIRef('http://linked.data.gov.au/dataset/corveg/site/site-54742'), None, None)):
        g2.add((s, p, o))

    # g2.serialize('site.jsonld', format='json-ld')

    result = g2.serialize(format='json-ld').decode('utf-8')
    result = json.loads(result)
    body = result[0]
    print(body)

    es = Elasticsearch()
    response = es.index(index='site', doc_type='_doc', id='http://linked.data.gov.au/dataset/corveg/site/site-54742', body=body)
