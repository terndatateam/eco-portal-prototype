SITES_QUERY = """
# Author: Edmond Chuc
 
# Purpose: This query provides the tabular data to create Elasticsearch documents for the TERN Ecological Data Portal.
 
# Improvements: Once the SHACL shape graphs have been reviewed and integrated into the ecological data pipeline, ensure that the optional fields are reflected in the SPARQL query by using OPTIONAL. This can be easily checked by running the query each time a new column is selected and ensure that the total number of sites are still retrieved (and not a subset of it).
 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX plot: <http://linked.data.gov.au/def/plot/>
PREFIX corveg: <http://linked.data.gov.au/dataset/corveg/>
PREFIX corveg-site: <http://linked.data.gov.au/dataset/corveg/site/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX locn: <http://www.w3.org/ns/locn#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX corveg-def: <http://linked.data.gov.au/def/corveg/>
PREFIX ssn-ext: <http://www.w3.org/ns/ssn/ext/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
SELECT ?site_id (STRAFTER(STR(?_project_id), "/linked.data.gov.au/dataset/corveg/site/p-") AS ?project_id) ?site_number ?site_label ?site_description ?site_visit_date ?site_database_date ?site_date_modified ?site_floristics_type ?site_sample_level ?site_sample_type ?location_description ?location_latitude ?location_longitude ?location_bioregion ?location_geographic_name ?location_date_modified ?location_mapscale ?location_mapsheet_name ?location_mapsheet_number ?location_method
WHERE {
    ?_site a plot:Site .
    
    ##################
    # Site Information
    ##################
    
    # Comment: The data for Site is a combination of the data from Site and Location table.
     
    # site_id
    ?_site dct:identifier ?site_id .
    filter (datatype(?site_id)=corveg-def:site-id) .
     
    # project_id
    ?_site prov:wasGeneratedBy ?_project_id .
     
    # site_number
    ?_site dct:identifier ?site_number .
    filter (datatype(?site_number)=corveg-def:site-number) .
     
    # site_label
    ?_site rdfs:label ?site_label .
     
    # site_description
    OPTIONAL { ?_site dct:description ?site_description . }
     
    # site_visit_date
    ?_site plot:siteDescription ?_observation_collection .
    ?_observation_collection sosa:phenomenonTime ?_phen_time .
    ?_phen_time time:inXSDDateTime ?site_visit_date .
     
    # site_database_date
    ?_observation_collection sosa:resultTime ?site_database_date .
     
    # site_date_modified
    ?_site dct:modified ?site_date_modified .
     
    # site_floristics_type
    ?_site plot:floristics ?site_floristics_type .
     
    # site_sample_level
    ?_site plot:sampleLevel ?site_sample_level .
     
    # site_sample_type
    ?_site plot:sampleType ?site_sample_type .
     
    # location_bioregion - TODO: the bioregion needs to be mapped to the IBRA controlled vocabulary. Match the bioregion on the skos:notation value.
    ?_site locn:location ?_location .
    ?_location ssn-ext:hasUltimateFeatureOfInterest ?location_bioregion .
    
    # location_latitude & location_longitude
    ?_location geosparql:hasGeometry ?_geo_point .
    ?_geo_point geo:lat ?location_latitude .
    ?_geo_point geo:long ?location_longitude .
    
    # location_description
    OPTIONAL { ?_location dct:description ?location_description . }
     
    # location_date_modified
    ?_location dct:modified ?location_date_modified .
    
    # location_geographic_name
    OPTIONAL { ?_location locn:geographicName ?location_geographic_name . }
    
    # location_mapscale
    ?_location plot:mapScale ?location_mapscale .
    
    # location_mapsheet_name
    OPTIONAL { ?_location plot:mapsheetName ?location_mapsheet_name . }
    
    # location_mapsshet_number
    OPTIONAL { ?_location plot:mapsheetNumber ?location_mapsheet_number . }
    
    # location_method
    ?_location plot:locationMethod ?location_method .
    
#    #########################
#    # Disturbance Information
#    #########################
#    
#    # Comment: The disturbance observations for a Site. 
#    
#    # Disturbance Observation Collection
#    ?dist_oc a ssn-ext:ObservationCollection .
#    ?dist_oc sosa:hasFeatureOfInterest ?_site .
#    ?dist_oc dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/1> .
#    ?dist_oc rdfs:label ?dist_oc_label .
}


"""

OGROUP_DISTURBANCE_QUERY = f"""
# Author: Edmond Chuc

# Purpose: Retrieve all Disturbance observations of a Site. 

PREFIX plot: <http://linked.data.gov.au/def/plot/>
PREFIX ssn-ext: <http://www.w3.org/ns/ssn/ext/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX time: <http://www.w3.org/2006/time#>
SELECT (?_observation as ?uri) ?label ?phenomenon_time ?observed_property ?method ?result ?disturbance_type
WHERE {{
    # observation_id
    ?_observation a sosa:Observation .
    
    # Filter on observation group type (Disturbance) and get the disturbance type.
    ?_observation_collection ssn-ext:hasMember ?_observation .
    ?_observation_collection dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/1> .
    ?_observation_collection dct:type ?disturbance_type .
    FILTER(regex(str(?disturbance_type), "http://linked.data.gov.au/def/corveg-cv/disturbance-type"))
    
    # label
    ?_observation rdfs:label ?label .
    
    # phenomenon_time
    ?_observation sosa:phenomenonTime ?_phenomenon_time .
    ?_phenomenon_time time:inXSDDateTime ?phenomenon_time .
    
    # observed_property
    ?_observation sosa:observedProperty ?observed_property .
    
    # method
    # Note: There are some observations with more than one procedure.
    ?_observation sosa:usedProcedure ?method .
    
    # result
    # Note: Some results are within a blank node. 
    # - This will be problematic. 
    # - How do we get all observations in one SPARQL query? 
    # - Is it possible? Do we have to write some logic and make multiple SPARQL queries for a single observation?
    {{
        ?_observation sosa:hasResult ?result .
        FILTER (!isBlank(?result))
    }}
#    UNION
#    {{
#        ?_observation sosa:hasResult ?_result .
#        FILTER (!isIRI(?_result))
##        ?_result 
#    }}
}}
#limit 100
"""