url = 'http://localhost:5000/?query=&floristics=http://linked.data.gov.au/def/corveg-cv/site-sample-floristics/c&next=true&page=2'

if '?page' in url:
    print('?page found')
elif '&page' in url:
    print('&page found')
    splits = url.split('&')
    for split in splits:
        if 'page' in split:
            url = url.replace(split, 'page=100')

print(url)

