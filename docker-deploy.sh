#!/bin/sh

docker pull ternau/plots-dev:latest
docker stop app
docker rm app
docker run --name app -d -p 8000:8000 ternau/plots-dev:latest
